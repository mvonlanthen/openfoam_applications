/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011-2013 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "timeVaryingMappedSampledGradientFvPatchField.H"
#include "Time.H"
#include "IFstream.H"
#include "OFstream.H"

// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

namespace Foam
{

// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

template<class Type>
timeVaryingMappedSampledGradientFvPatchField<Type>::
timeVaryingMappedSampledGradientFvPatchField
(
    const fvPatch& p,
    const DimensionedField<Type, volMesh>& iF
)
:
    fixedGradientFvPatchField<Type>(p, iF),
    fieldTableName_(iF.name()),
    perturb_(0),
    sampleTimes_(0),
    startSampleTime_(-1),
    startSampledValues_(0),
    endSampleTime_(-1),
    endSampledValues_(0)
    //offset_()
{}


template<class Type>
timeVaryingMappedSampledGradientFvPatchField<Type>::
timeVaryingMappedSampledGradientFvPatchField
(
    const timeVaryingMappedSampledGradientFvPatchField<Type>& ptf,
    const fvPatch& p,
    const DimensionedField<Type, volMesh>& iF,
    const fvPatchFieldMapper& mapper
)
:
    fixedGradientFvPatchField<Type>(ptf, p, iF, mapper),
    fieldTableName_(ptf.fieldTableName_),
    perturb_(ptf.perturb_),
    mapperPtr_(ptf.mapperPtr_),
    sampleTimes_(0),
    startSampleTime_(-1),
    startSampledValues_(0),
    endSampleTime_(-1),
    endSampledValues_(0)
    //offset_(ptf.offset_().clone().ptr())
{}


template<class Type>
timeVaryingMappedSampledGradientFvPatchField<Type>::
timeVaryingMappedSampledGradientFvPatchField
(
    const fvPatch& p,
    const DimensionedField<Type, volMesh>& iF,
    const dictionary& dict
)
:
    fixedGradientFvPatchField<Type>(p, iF),
    fieldTableName_(iF.name()),
    perturb_(dict.lookupOrDefault("perturb", 1e-5)),
    mapperPtr_(NULL),
    sampleTimes_(0),
    startSampleTime_(-1),
    startSampledValues_(0),
    endSampleTime_(-1),
    endSampledValues_(0)
    //offset_(DataEntry<Type>::New("offset", dict))
{
    dict.readIfPresent("fieldTableName", fieldTableName_);

    if (dict.found("value"))
    {
        fvPatchField<Type>::operator==(Field<Type>("value", dict, p.size()));
    }
    else
    {
        updateCoeffs();
    }
}


template<class Type>
timeVaryingMappedSampledGradientFvPatchField<Type>::
timeVaryingMappedSampledGradientFvPatchField
(
    const timeVaryingMappedSampledGradientFvPatchField<Type>& ptf
)
:
    fixedGradientFvPatchField<Type>(ptf),
    fieldTableName_(ptf.fieldTableName_),
    perturb_(ptf.perturb_),
    mapperPtr_(ptf.mapperPtr_),
    sampleTimes_(ptf.sampleTimes_),
    startSampleTime_(ptf.startSampleTime_),
    startSampledValues_(ptf.startSampledValues_),
    endSampleTime_(ptf.endSampleTime_),
    endSampledValues_(ptf.endSampledValues_)
    //offset_(ptf.offset_().clone().ptr())
{}



template<class Type>
timeVaryingMappedSampledGradientFvPatchField<Type>::
timeVaryingMappedSampledGradientFvPatchField
(
    const timeVaryingMappedSampledGradientFvPatchField<Type>& ptf,
    const DimensionedField<Type, volMesh>& iF
)
:
    fixedGradientFvPatchField<Type>(ptf, iF),
    fieldTableName_(ptf.fieldTableName_),
    perturb_(ptf.perturb_),
    mapperPtr_(ptf.mapperPtr_),
    sampleTimes_(ptf.sampleTimes_),
    startSampleTime_(ptf.startSampleTime_),
    startSampledValues_(ptf.startSampledValues_),
    endSampleTime_(ptf.endSampleTime_),
    endSampledValues_(ptf.endSampledValues_)
    //offset_(ptf.offset_().clone().ptr())
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

template<class Type>
void timeVaryingMappedSampledGradientFvPatchField<Type>::autoMap
(
    const fvPatchFieldMapper& m
)
{
    fixedGradientFvPatchField<Type>::autoMap(m);
    if (startSampledValues_.size())
    {
        startSampledValues_.autoMap(m);
        endSampledValues_.autoMap(m);
    }
    // Clear interpolator
    mapperPtr_.clear();
    startSampleTime_ = -1;
    endSampleTime_ = -1;
}


template<class Type>
void timeVaryingMappedSampledGradientFvPatchField<Type>::rmap
(
    const fvPatchField<Type>& ptf,
    const labelList& addr
)
{
    fixedGradientFvPatchField<Type>::rmap(ptf, addr);

    const timeVaryingMappedSampledGradientFvPatchField<Type>& tiptf =
        refCast<const timeVaryingMappedSampledGradientFvPatchField<Type> >(ptf);

    startSampledValues_.rmap(tiptf.startSampledValues_, addr);
    endSampledValues_.rmap(tiptf.endSampledValues_, addr);

    // Clear interpolator
    mapperPtr_.clear();
    startSampleTime_ = -1;
    endSampleTime_ = -1;
}


template<class Type>
void timeVaryingMappedSampledGradientFvPatchField<Type>::checkTable()
{
    // Initialise
    if (mapperPtr_.empty())
    {

        IOobject samplePointsHeader
        (
            "points",
            this->db().time().constant(),
            "boundaryData"/this->patch().name(),
            this->db(),
            IOobject::MUST_READ,
            IOobject::AUTO_WRITE,
            false
        );

        IOField<point> samplePoints
        (
            IOobject
            (
                "points",
                this->db().time().constant(),
                "boundaryData"/this->patch().name(),
                this->db(),
                IOobject::NO_READ,
                IOobject::AUTO_WRITE,
                false
            )
        );

        if (samplePointsHeader.headerOk())
        {
            samplePoints = samplePointsHeader;
        }
        else
        {
            vectorField readPoints;
            IFstream dataStream(samplePoints.filePath());
            dataStream >> readPoints;
            samplePoints = readPoints;
        }

        const fileName samplePointsFile = samplePoints.filePath();

        if (debug)
        {
            Info<< "timeVaryingMappedSampledGradientFvPatchField :"
                << " Read " << samplePoints.size() << " sample points from "
                << samplePointsFile << endl;
        }

        // Allocate the interpolator
        mapperPtr_.reset  //something long is done here
        (
            new pointToPointPlanarInterpolation
            (
                samplePoints,
                 this->patch().patch().faceCentres(),
                perturb_
            )
        );

        // Read the times for which data is available
        const fileName samplePointsDir = samplePointsFile.path();
        sampleTimes_ = Time::findTimes(samplePointsDir);

        if (debug)
        {
            Info<< "timeVaryingMappedSampledGradientFvPatchField : In directory "
                << samplePointsDir << " found times "
                << pointToPointPlanarInterpolation::timeNames(sampleTimes_)
                << endl;
        }
    }


    // Find current time in sampleTimes
    label lo = -1;
    label hi = -1;

    bool foundTime = mapperPtr_().findTime
    (
        sampleTimes_,
        startSampleTime_,
        this->db().time().value(),
        lo,
        hi
    );

    if (!foundTime)
    {
        FatalErrorIn
        (
            "timeVaryingMappedSampledGradientFvPatchField<Type>::checkTable()"
        )   << "Cannot find starting sampling values for current time "
            << this->db().time().value() << nl
            << "Have sampling values for times "
            << pointToPointPlanarInterpolation::timeNames(sampleTimes_) << nl
            << "In directory "
            <<  this->db().time().constant()/"boundaryData"/this->patch().name()
            << "\n    on patch " << this->patch().name()
            << " of field " << fieldTableName_
            << exit(FatalError);
    }


    // Update sampled data fields.

    if (lo != startSampleTime_)
    {
        startSampleTime_ = lo;

        if (startSampleTime_ == endSampleTime_)
        {
            // No need to reread since are end values
            if (debug)
            {
                Pout<< "checkTable : Setting startValues to (already read) "
                    <<   "boundaryData"
                        /this->patch().name()
                        /sampleTimes_[startSampleTime_].name()
                    << endl;
            }
            startSampledValues_ = endSampledValues_;
        }
        else
        {
            if (debug)
            {
                Pout<< "checkTable : Reading startValues from "
                    <<   "boundaryData"
                        /this->patch().name()
                        /sampleTimes_[lo].name()
                    << endl;
            }

            // Reread values and interpolate
            IOobject valsHeader
            (
                fieldTableName_,
                this->db().time().constant(),
                "boundaryData"
               /this->patch().name()
               /sampleTimes_[startSampleTime_].name(),
                this->db(),
                IOobject::MUST_READ,
                IOobject::AUTO_WRITE,
                false
            );

            IOField<Type> vals
            (
                IOobject
                (
                    fieldTableName_,
                    this->db().time().constant(),
                    "boundaryData"
                   /this->patch().name()
                   /sampleTimes_[startSampleTime_].name(),
                    this->db(),
                    IOobject::NO_READ,
                    IOobject::AUTO_WRITE,
                    false
                )
            );

            if (valsHeader.headerOk())
            {
                vals = valsHeader;
            }
            else
            {
                Field<Type> readVals;
                IFstream dataStream(vals.filePath());
                dataStream >> readVals;
                vals = readVals;
            }

            if (vals.size() != mapperPtr_().sourceSize())
            {
                FatalErrorIn
                (
                    "timeVaryingMappedSampledGradientFvPatchField<Type>::"
                    "checkTable()"
                )   << "Number of values (" << vals.size()
                    << ") differs from the number of points ("
                    <<  mapperPtr_().sourceSize()
                    << ") in file " << vals.objectPath() << exit(FatalError);
            }

            startSampledValues_ = mapperPtr_().interpolate(vals);
        }
    }

    if (hi != endSampleTime_)
    {
        endSampleTime_ = hi;

        if (endSampleTime_ == -1)
        {
            // endTime no longer valid. Might as well clear endValues.
            if (debug)
            {
                Pout<< "checkTable : Clearing endValues" << endl;
            }
            endSampledValues_.clear();
        }
        else
        {
            if (debug)
            {
                Pout<< "checkTable : Reading endValues from "
                    <<   "boundaryData"
                        /this->patch().name()
                        /sampleTimes_[endSampleTime_].name()
                    << endl;
            }

            // Reread values and interpolate
            IOobject valsHeader
            (
                fieldTableName_,
                this->db().time().constant(),
                "boundaryData"
               /this->patch().name()
               /sampleTimes_[startSampleTime_].name(),
                this->db(),
                IOobject::MUST_READ,
                IOobject::AUTO_WRITE,
                false
            );

            IOField<Type> vals
            (
                IOobject
                (
                    fieldTableName_,
                    this->db().time().constant(),
                    "boundaryData"
                   /this->patch().name()
                   /sampleTimes_[startSampleTime_].name(),
                    this->db(),
                    IOobject::NO_READ,
                    IOobject::AUTO_WRITE,
                    false
                )
            );

            if (valsHeader.headerOk())
            {
                vals = valsHeader;
            }
            else
            {
                Field<Type> readVals;
                IFstream dataStream(vals.filePath());
                dataStream >> readVals;
                vals = readVals;
            }

            if (vals.size() != mapperPtr_().sourceSize())
            {
                FatalErrorIn
                (
                    "timeVaryingMappedSampledGradientFvPatchField<Type>::"
                    "checkTable()"
                )   << "Number of values (" << vals.size()
                    << ") differs from the number of points ("
                    <<  mapperPtr_().sourceSize()
                    << ") in file " << vals.objectPath() << exit(FatalError);
            }

            endSampledValues_ = mapperPtr_().interpolate(vals);
        }
    }
}


template<class Type>
void timeVaryingMappedSampledGradientFvPatchField<Type>::updateCoeffs()
{
    if (this->updated())
    {
        return;
    }

    Info << "patch name: " << this->patch().name() << endl;

    checkTable();

    // Interpolate between the sampled data
    if (endSampleTime_ == -1)
    {
        // only start value
        if (debug)
        {
            Pout<< "updateCoeffs : Sampled, non-interpolated values"
                << " from start time:"
                << sampleTimes_[startSampleTime_].name() << nl;
        }

        this->operator==(startSampledValues_);
    }
    else
    {
        scalar start = sampleTimes_[startSampleTime_].value();
        scalar end = sampleTimes_[endSampleTime_].value();

        scalar s = (this->db().time().value() - start)/(end - start);

        if (debug)
        {
            Pout<< "updateCoeffs : Sampled, interpolated values"
                << " between start time:"
                << sampleTimes_[startSampleTime_].name()
                << " and end time:" << sampleTimes_[endSampleTime_].name()
                << " with weight:" << s << endl;
        }

        this->operator==((1 - s)*startSampledValues_ + s*endSampledValues_);
    }

    // apply offset to mapped values
    //const scalar t = this->db().time().timeOutputValue();
    //this->operator==(*this + offset_->value(t));

    if (debug)
    {
        Pout<< "updateCoeffs : set fixedValue to min:" << gMin(*this)
            << " max:" << gMax(*this) << endl;
    }

    //fixedValueFvPatchField<Type>::updateCoeffs();
    fixedGradientFvPatchField<Type>::updateCoeffs();
}


template<class Type>
void timeVaryingMappedSampledGradientFvPatchField<Type>::write(Ostream& os) const
{
    fvPatchField<Type>::write(os);
    os.writeKeyword("perturb") << perturb_ << token::END_STATEMENT << nl;

    if (fieldTableName_ != this->dimensionedInternalField().name())
    {
        os.writeKeyword("fieldTableName") << fieldTableName_
            << token::END_STATEMENT << nl;
    }

    //offset_->writeData(os);

    this->writeEntry("value", os);
}


// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

} // End namespace Foam

// ************************************************************************* //
